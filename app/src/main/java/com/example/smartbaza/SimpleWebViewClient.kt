package com.example.smartbaza

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.widget.NestedScrollView

class SimpleWebViewClient(activity: Activity?) : WebViewClient() {
    private var activity: Activity? = null

    override fun shouldOverrideUrlLoading(

        webView: WebView,
        url: String
    ): Boolean {

        if (url.contains("smartbazar.kz")) {
            return false
        }
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        webView.settings.allowFileAccess = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        activity!!.startActivity(intent)

        return true
    }

    init {
        this.activity = activity
    }
}