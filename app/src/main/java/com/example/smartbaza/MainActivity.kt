package com.example.smartbaza


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.example.smartbaza.SimpleWebViewClient
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView


@SuppressLint("SetJavaScriptEnabled")
class MainActivity : AppCompatActivity() {
    var mySwipeRefreshLayout: SwipeRefreshLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_home, R.id.navigation_catalog, R.id.navigation_cart, R.id.navigation_favorites, R.id.navigation_orders))

        setupActionBarWithNavController(navController, appBarConfiguration)
        val navHome = findViewById<BottomNavigationItemView>(R.id.navigation_home)
        navView.setupWithNavController(navController)
        NavigationUI.setupActionBarWithNavController(this, navController,appBarConfiguration)
        NavigationUI.setupWithNavController(navView, navController)


        val mWebView = findViewById<WebView>(R.id.webView)
        val webSettings = mWebView.settings
        webSettings.javaScriptEnabled = true
        val webViewClient = SimpleWebViewClient(this)

        mWebView.webViewClient = webViewClient

        mWebView.loadUrl("https://smartbazar.kz/")


        navView.setOnNavigationItemSelectedListener {
            when(it.itemId){

                R.id.navigation_home->{
                    mWebView.loadUrl("https://smartbazar.kz/")

                }
                R.id.navigation_catalog->{
                    mWebView.loadUrl("https://smartbazar.kz/products/43")

                }
                R.id.navigation_cart->{
                    mWebView.loadUrl("https://smartbazar.kz/products/92")
                    }
                R.id.navigation_favorites->{
                    mWebView.loadUrl("https://smartbazar.kz/products/84")
                    }
                R.id.navigation_orders->{
                    mWebView.loadUrl("https://smartbazar.kz/products/59")

                }
                }
            true
            }

        mySwipeRefreshLayout =
            findViewById<View>(R.id.swipeContainer) as SwipeRefreshLayout

        mySwipeRefreshLayout!!.setOnRefreshListener(
            OnRefreshListener { mWebView.reload()
                                mySwipeRefreshLayout!!.isRefreshing = false}

        )



    }



}

